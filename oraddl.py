#!/usr/bin/python
'''
oraddl.py
=========

Requirements:
  * python 2.4
  * cx_Oracle
  * argparse

Server-side installation:
  $sqlplus user/pass@db @oraddl_metadata.sql
  $sqlplus user/pass@db @oraddl.sql

(c) Pavel Popov, 2012.
GPLv2 Lisence applied.
'''

import sys
import os
import shutil
import argparse
from os.path import join
import cx_Oracle

VERSION=0.1
DESCRIPTION='tool for exporting DDL\'s of Oracle schema objects.'

def _rmdir(path):
  if os.path.isdir(path):
    shutil.rmtree(path)

def _mkdir(path):
  if not os.path.exists(path):
    os.makedirs(path)

def install(connection):
  pass

def main():

  #arguments

  argparser = argparse.ArgumentParser(description='%s v%s - %s' %(__file__,VERSION,DESCRIPTION))

  argparser.add_argument('--credentials', required=True,
    help='Credentials for database access in form user/pass@db')
  argparser.add_argument('--filter', default='1=1',
    help='SQL filter, used in querying result in the where clause. Supported fiels are obj_type and obj_name')
  argparser.add_argument('--order', default='uk',
    help='SQL order by statement, used in querying result in the order by clause. Supported fiels are obj_type and obj_name')
  argparser.add_argument('--output_dir', default='output',
    help='Output directory, where files will be placed. Will be emptied before export')
  argparser.add_argument('--download_only', const=True, default=False, action='store_const',
    help='If specified, then script will only download metadata rather then creating it and then downloading')

  args = argparser.parse_args()

  credentials = args.credentials
  dest_folder = args.output_dir
  ddl_filter = args.filter
  ddl_order = args.order
  download_only = args.download_only

  _rmdir(dest_folder)
  _mkdir(dest_folder)
  current_dir = dest_folder

  build_all = open(join(dest_folder,'build_all.sql'),'w')

  connection = cx_Oracle.connect(credentials)
  connection.autocommit = 0

  cursor = connection.cursor();

  if not download_only:
    cursor.execute('delete from oraddl_metadata')
    cursor.execute('begin oraddl; end;')
    cursor.execute('commit')

  cursor.execute('''
    select obj_type,
           obj_name,
           md 
      from oraddl_metadata 
     where %s 
     order by %s, uk''' % (ddl_filter, ddl_order))

  for row in cursor:
    current_dir = join(dest_folder,row[0])
    _mkdir(current_dir)

    filename = join(current_dir,'%s.%s' % (row[1],'sql'))

    ddl_file = open(filename,'w')
    ddl_file.write(row[2].read())
    ddl_file.close()

    build_all.write('@%s.sql;\n'%join(row[0],row[1]))

    print 'Processed file: %s' %filename
    #print row

  connection.close()
  build_all.close()

if __name__ == '__main__':
  main()
