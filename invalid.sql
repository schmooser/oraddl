break on c1 skip 2

set pages 999

col c1 heading 'type' format a40
col c2 heading 'name' format a30

ttitle 'Invalid Objects'

select 
   object_type c1,
   object_name c2
from 
   user_objects 
where 
   status != 'VALID'
order by 
   object_type, object_name
;

exit;
